-- pandoc insert gollum wiki style heading from meta

local filenames = {}

local function get_meta(m)
    if m.filenames then
        -- remove file suffix
        for k,v in pairs(m.filenames) do
            local filename, extension = string.match(v, "(.*)(%..+)")
            if extension then
                filenames[filename] = extension
            end
        end
    end
end

-- turn a link target into a pandoc auto_identifier
-- http://pandoc.org/MANUAL.html#extension-auto_identifiers
-- Very likely mistakes here!
local function linkify(s)
    -- Remove all formatting, links, etc.
    -- Remove all footnotes.
    local s1
    if type(s) == "string" then
        s1 = s
    else
        s1 = string.lower(pandoc.stringify(s))
    end
    -- Replace all spaces and newlines with hyphens.
    local s2 = string.gsub(s1, "%s", "-")
    -- Remove all punctuation, except underscores, hyphens, and periods.
    local s3 = string.gsub(s2, "[^%w_%-%.]", "")
    --Convert all alphabetic characters to lowercase.
    local s4 = string.lower(s3)
    -- Remove everything up to the first letter (identifiers may not begin with a number or punctuation mark).
    local discard, s5 = string.match(s4, "(%A*-)(.*)")
    if not discard then
        s5 = s4
    end
    if not s5 then
        s5 = ""
    end
    return s5
end

-- https://stackoverflow.com/questions/48169995/pandoc-how-to-link-to-a-section-in-another-markdown-file
-- does not work with relative links...
local function rewrite_combined_link(link)
    local target = link.target
    if not string.match(target, "://") and
        filenames[target] then
        local linktarget = linkify(target)
        link.target = "#" .. linktarget
        return link
    end
end

-- https://stackoverflow.com/questions/21247978/compile-multiple-files-into-one-with-title-blocks
-- https://gist.github.com/mathematicalcoffee/e4f25350449e6004014f
-- https://stackoverflow.com/questions/47350104/pandoc-filter-in-lua-and-walk-block
local function insert_meta_header(doc, meta, var)
    local header_contents = {}
    if doc.meta.title then
        table.insert(
            header_contents,
            pandoc.HorizontalRule ()
        )
        local title = pandoc.Header(1, doc.meta.title)
        table.insert(
            header_contents,
            title
        )
        table.insert(
            header_contents,
            pandoc.HorizontalRule ()
        )
        if doc.meta.author then
            local byline = "Last edited by "
            if type(doc.meta.author) == "table" then
                byline = byline .. table.concat(
                    doc.meta.author,
                    '; '
                )
            elseif type(doc.meta.author) == "string" then
                byline = byline .. doc.meta.author
            end
            if doc.meta.date then
                byline = byline .. " " .. doc.meta.date
            end
            if doc.meta.revcount then
                byline = byline .. ". " .. doc.meta.revcount
                byline = byline .. " revisions."
            end

            local byline_block = pandoc.Plain(pandoc.Str(
                byline
            ))
            table.insert(
                header_contents,
                3,
                byline_block
            )
        end
        local blocks = header_contents
        for i=1,#doc.blocks do
            blocks[#blocks+1] = doc.blocks[i]
        end
        return pandoc.Pandoc(blocks, doc.meta)
    end
end

-- Inline -> Blocks -> Meta -> Pandoc
-- Need Meta first
return {
    {Meta = get_meta},
    {Link = rewrite_combined_link},
    {Pandoc = insert_meta_header}
}

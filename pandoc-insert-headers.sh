#!/bin/bash

# Add Title, git author, git date information as headings to documents
# Usage:
# bash
# FILELIST=(home.md hardware.md)
# FILELIST=($(find -mindepth 1 -path '.git' -prune -or -iname '*.md' -printf '%P\n'))
# pandoc-insert-headers.sh "${FILELIST[@]}" | \
# pandoc --standalone --table-of-contents \
# --from gfm --pdf-engine=xelatex --out docs.pdf

WORKING_FORMAT=gfm
FLAGS=('--table-of-contents')

PAGE_ORDER=(
"$@"
)

for f in "${PAGE_ORDER[@]}"; do
  date=$(git log --max-count=1 --format="%ad" --date=short -- "$f")
  last_author=$(git log --max-count=1 --format="%aN" --date=short -- "$f")
  revcount=$(git rev-list --count HEAD -- "$f")
  #IFS=$'\n'
  #authors=($(git shortlog -s -n "$f" | sed -e 's:.*\t::'))
  #$(printf -- "--metadata=author:'%s' " "${authors[@]}") \
  #IFS=$' \t\n'
  pandoc \
    "${FLAGS[@]}" \
    --write="$WORKING_FORMAT" \
    --metadata=title:"${f%.md}" \
    --metadata=date:"$date" \
    --metadata=author:"$last_author" \
    --metadata=revcount:"$revcount" \
    $(printf -- "--metadata=filenames:'%s' " "${PAGE_ORDER[@]}") \
    --lua-filter pandoc-meta-header.lua \
    "$f"
  echo ""
done

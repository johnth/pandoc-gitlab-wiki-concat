# GitLab wiki as combined Pandoc document

# Usage

```bash
pandoc-insert-headers.sh file1.md file2.md | \
pandoc --standalone --table-of-contents \
--from gfm --pdf-engine=xelatex --out wiki.pdf
```

```bash
FILELIST=(home.md hardware.md)
FILELIST=($(find -mindepth 1 -path '.git' -prune -or -iname '*.md' -printf '%P\n'))
pandoc-insert-headers.sh "${FILELIST[@]}" | \
pandoc --standalone --table-of-contents \
--from gfm --pdf-engine=xelatex --out docs.pdf
```

# To do

- de-prioritise headers under filename header for pretty PDF outlines
- list of links at the bottom of each file

# Why?

- Have a GitLab wiki.
- Want to concatenate some wiki pages into a single file,
  with intra wiki links pointing to the correct section of the file.
